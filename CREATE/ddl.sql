-- Version 1.0.0 --

create database if not exists only_tabelas;

create table if not exists only_tabelas.jogo (
    id int not null auto_increment,
    nome varchar(120) not null,
    unique (nome),
    constraint pk_jogo primary key (id)
);

create table if not exists only_tabelas.tabela (
    id int not null auto_increment,
    nome varchar(255) not null,
    coluna_1 varchar(120) not null,
    coluna_2 varchar(120) not null,
    jogo_id int not null,
    constraint pk_tabela primary key (id),
    constraint fk_tabela_jogo foreign key (jogo_id) references only_tabelas.jogo(id)
);

create table if not exists only_tabelas.valor (
    id int not null auto_increment,
    valor_coluna_1 text not null,
    valor_coluna_2 text not null,
    tabela_id int not null,
    constraint pk_valor primary key (id),
    constraint fk_valor_tabela foreign key (tabela_id) references only_tabelas.tabela(id)
);
